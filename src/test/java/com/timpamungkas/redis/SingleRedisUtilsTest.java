package com.timpamungkas.redis;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class SingleRedisUtilsTest {

	private RedisUtils redisUtils;

	public SingleRedisUtilsTest() {
		try {
			this.redisUtils = new SingleRedisUtils().configYAML("redis-single.yaml").build();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	void testSingleRedisUtils() {
		assertNotNull(this.redisUtils);
	}

	@Test
	void testClient() {
		assertNotNull(this.redisUtils.client());
	}

	@Test
	void testShutdown() {
		redisUtils.shutdown();
		assertTrue(redisUtils.client().isShuttingDown() || redisUtils.client().isShutdown());
	}

}
