package com.timpamungkas.collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

class CollectionUtilsTest {

	@Test
	void testIsEmpty() {
		assertTrue(CollectionUtils.isEmpty(Collections.EMPTY_LIST));
		assertTrue(CollectionUtils.isEmpty(Collections.EMPTY_SET));
		assertTrue(CollectionUtils.isEmpty(Collections.EMPTY_MAP));

		List<String> list = Arrays.asList("1", "2", "3");

		Set<Integer> set = new HashSet<>();
		set.add(1);
		set.add(2);
		set.add(3);

		Map<Integer, String> map = new HashMap<>();
		map.put(1, "one");
		map.put(2, "two");

		assertFalse(CollectionUtils.isEmpty(list));
		assertFalse(CollectionUtils.isEmpty(set));
		assertFalse(CollectionUtils.isEmpty(map));
	}

	@Test
	void testIsContainsEmptyString() {
		List<String> trueList = Arrays.asList("1", "2", "3");
		List<String> falseList = Arrays.asList("1", "2", "", "4");

		assertFalse(CollectionUtils.isContainsEmptyString(trueList));
		assertTrue(CollectionUtils.isContainsEmptyString(falseList));
	}

	@Test
	void testIsContainsEmptyString_Performance() {
		List<String> list = new ArrayList<>();

		for (long i = 0; i < 5000000; i++) {
			list.add(Long.toString(i));
		}

		list.add(StringUtils.EMPTY);
		Collections.shuffle(list);

		long start, millis_1, millis_2;
		boolean result_1, result_2;

		start = System.currentTimeMillis();
		result_1 = CollectionUtils.isContainsEmptyString(list);
		millis_1 = System.currentTimeMillis() - start;

		start = System.currentTimeMillis();
		result_2 = testMethodPerformance(list);
		millis_2 = System.currentTimeMillis() - start;

		assertEquals(result_1, result_2);
		assertTrue(millis_1 < millis_2);
	}

	private boolean testMethodPerformance(List<String> list) {
		for (String s : list) {
			if (s.isEmpty()) {
				return true;
			}
		}

		return false;
	}

}
