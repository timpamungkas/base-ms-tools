package com.timpamungkas.security;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class HmacUtilsTest {

	@Test
	void testCalculateHMAC() {
		HmacUtils hmacUtils = new HmacUtils();
		String secret = "key";
		
		String data = "The quick brown fox jumps over the lazy dog";
		String expected = "f7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8";
		String actual = hmacUtils.calculateHMAC(secret, data);
		assertEquals(expected, actual);

		data = "A big blue bird fly across a wide blue ocean";
		expected = "3a6923a83e63aa5f31f1344c8bccbd37342bf4d0c4317950286ecc5f7de1eda7";
		actual = hmacUtils.calculateHMAC(secret, data);
		assertEquals(expected, actual);
	}

}
