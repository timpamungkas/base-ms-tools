package com.timpamungkas.collection;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Set of collection helper method. Instead of using apache collection / guava,
 * use this class to reduce dependency & size.
 */
public class CollectionUtils {

	public static boolean isContainsEmptyString(final Collection<String> coll) {
		return isEmpty(coll) || coll.contains(StringUtils.EMPTY);
	}

	public static boolean isEmpty(final Collection<?> coll) {
		return coll == null || coll.isEmpty();
	}

	public static boolean isEmpty(final Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

}
