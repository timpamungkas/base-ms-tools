package com.timpamungkas.redis;

import org.redisson.api.RedissonClient;

public abstract class RedisUtils {

	protected RedissonClient client;

	/**
	 * Get redis client.
	 * 
	 * @return redis client.
	 */
	public RedissonClient client() {
		return this.client;
	}

	public void shutdown() {
		client().shutdown();
	}

}
