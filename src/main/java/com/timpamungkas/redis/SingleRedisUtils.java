package com.timpamungkas.redis;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.redisson.Redisson;
import org.redisson.config.Config;

public class SingleRedisUtils extends RedisUtils {

	private Config config;

	public SingleRedisUtils() {
	}

	/**
	 * Set address of single redis instance.
	 * 
	 * @param redisAddress
	 *            Redis address in the form of <code>address:port</code>, example
	 *            <code>127.0.0.1:6379</code>
	 * @return
	 */
	public SingleRedisUtils address(String redisAddress) {
		this.config.useSingleServer().setAddress("redis://".concat(redisAddress));
		return this;
	}

	/**
	 * Build redis client based on config.
	 * 
	 * @return this instance with redis client ready to use. Use the client using
	 *         {@link SingleRedisUtils#client() client} method.
	 */
	public SingleRedisUtils build() {
		this.client = Redisson.create(this.config);
		return this;
	}

	/**
	 * Build config from JSON file. For JSON content, see <a href=
	 * "https://github.com/redisson/redisson/wiki/2.-Configuration#261-single-instance-settings"
	 * target="_blank">here</a>
	 *
	 * @param file
	 *            path to config file
	 * @return this instance
	 * @throws IOException
	 *             if <code>resourcePath</code> is invalid
	 */
	public SingleRedisUtils configJSON(File file) throws IOException {
		this.config = Config.fromJSON(file);
		return this;
	}

	/**
	 * Build config from JSON file. For JSON content, see <a href=
	 * "https://github.com/redisson/redisson/wiki/2.-Configuration#261-single-instance-settings"
	 * target="_blank">here</a>
	 * 
	 * @param resourcePath
	 *            resource name within <code>src/main/resources</code>
	 * @return this instance
	 * @throws IOException
	 *             if <code>resourcePath</code> is invalid
	 */
	public SingleRedisUtils configJSON(String resourcePath) throws IOException {
		this.config = Config.fromJSON(Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath));
		return this;
	}

	/**
	 * Build config from JSON file. For JSON content, see <a href=
	 * "https://github.com/redisson/redisson/wiki/2.-Configuration#261-single-instance-settings"
	 * target="_blank">here</a>
	 *
	 * @param url
	 *            url to config file
	 * @return this instance
	 * @throws IOException
	 *             if <code>resourcePath</code> is invalid
	 */
	public SingleRedisUtils configJSON(URL url) throws IOException {
		this.config = Config.fromJSON(url);
		return this;
	}

	/**
	 * Build config from YAML file. For YAML content, see <a href=
	 * "https://github.com/redisson/redisson/wiki/2.-Configuration#261-single-instance-settings"
	 * target="_blank">here</a>
	 *
	 * @param file
	 *            path to config file
	 * @return this instance
	 * @throws IOException
	 *             if <code>resourcePath</code> is invalid
	 */
	public SingleRedisUtils configYAML(File file) throws IOException {
		this.config = Config.fromYAML(file);
		return this;
	}

	/**
	 * Build config from YAML file. For YAML content, see <a href=
	 * "https://github.com/redisson/redisson/wiki/2.-Configuration#261-single-instance-settings"
	 * target="_blank">here</a>
	 * 
	 * @param resourcePath
	 *            resource name within <code>src/main/resources</code>
	 * @return this instance
	 * @throws IOException
	 *             if <code>resourcePath</code> is invalid
	 */
	public SingleRedisUtils configYAML(String resourcePath) throws IOException {
		this.config = Config.fromYAML(Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath));
		return this;
	}

	/**
	 * Build config from YAML file. For YAML content, see <a href=
	 * "https://github.com/redisson/redisson/wiki/2.-Configuration#261-single-instance-settings"
	 * target="_blank">here</a>
	 *
	 * @param url
	 *            url to config file
	 * @return this instance
	 * @throws IOException
	 *             if <code>resourcePath</code> is invalid
	 */
	public SingleRedisUtils configYAML(URL url) throws IOException {
		this.config = Config.fromYAML(url);
		return this;
	}

	/**
	 * Number of threads amount shared across all listeners of <code>RTopic</code>
	 * object, invocation handlers of <code>RRemoteService</code> object and
	 * <code>RExecutorService</code> tasks. <br/>
	 * Default value (0) = current_cores_amount * 2
	 * 
	 * @param threads
	 *            number of threads
	 * @return this instance
	 */
	public SingleRedisUtils setThreads(int threads) {
		this.config.setThreads(threads);
		return this;
	}

}
