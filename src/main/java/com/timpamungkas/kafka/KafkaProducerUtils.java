package com.timpamungkas.kafka;

import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.Validate;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.connect.json.JsonSerializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.timpamungkas.collection.CollectionUtils;
import com.timpamungkas.date.DateUtils;

/**
 * Helper class for sending value to kafka as JSON payload.
 * 
 * @author Timotius Pamungkas
 *
 * @param <T>
 *            Class to send as payload.
 */
public class KafkaProducerUtils<T> {

	private static Producer<String, JsonNode> createJsonProducer(List<String> bootstrapServers, String clientId) {
		Validate.notEmpty(clientId, "clientID must not be empty");

		bootstrapServers.removeIf(String::isEmpty);
		if (CollectionUtils.isEmpty(bootstrapServers)) {
			throw new IllegalArgumentException("bootstrapServers must not be empty");
		}

		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, String.join(",", bootstrapServers));
		props.put(ProducerConfig.CLIENT_ID_CONFIG, clientId);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class.getName());

		return new KafkaProducer<>(props);
	}

	private Producer<String, JsonNode> kafkaProducer;

	public KafkaProducerUtils(List<String> bootstrapServers, String clientId) {
		this.kafkaProducer = createJsonProducer(bootstrapServers, clientId);
	}

	public void close() {
		this.kafkaProducer.close();
	}

	public void sendAsJson(String topic, T value) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setDateFormat(DateUtils.DATE_TIME_FORMAT_WITH_MILLISECOND);

			JsonNode jsonNode = objectMapper.valueToTree(value);

			ProducerRecord<String, JsonNode> rec = new ProducerRecord<>(topic, jsonNode);
			kafkaProducer.send(rec);
		} finally {
			kafkaProducer.flush();
		}
	}

}
