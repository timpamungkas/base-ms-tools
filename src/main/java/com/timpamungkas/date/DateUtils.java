package com.timpamungkas.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
	public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	public static final DateFormat DATE_TIME_FORMAT_WITH_MILLISECOND = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");

}
