package com.timpamungkas.security;

import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HmacUtils {

	private static final String HMAC_ALGORITHM = "HmacSHA256";

	public String calculateHMAC(String secret, String data) {
		String digest = null;

		try {
			SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(), HMAC_ALGORITHM);
			Mac mac = Mac.getInstance(HMAC_ALGORITHM);
			mac.init(signingKey);
			byte[] rawHmac = mac.doFinal(data.getBytes());
			StringBuilder hash = new StringBuilder();

			for (int i = 0; i < rawHmac.length; i++) {
				String hex = Integer.toHexString(0xFF & rawHmac[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			digest = hash.toString();
		} catch (GeneralSecurityException e) {
			throw new IllegalArgumentException();
		}

		return digest;
	}

}
